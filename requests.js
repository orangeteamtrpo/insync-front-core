
//import { fetchEventSource } from '@microsoft/fetch-event-source';

backend_url = "https://orangeteam-insync-dev.herokuapp.com/";

// returns User if register successful, null otherwise
async function registerRequest(username, email, password) {
    data={'username': username, 'email': email, 'password' : password}
    return fetch(backend_url + 'register', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.status == 409) {
            console.log("User exists");
            return null;
        }
        return response.json();
    })
    .catch(function(err) {
        console.info("err:", err );
    });
}

// returns User if login successful, null otherwise
async function loginRequest(email, password) {
    data={'email': email, 'password' : password}
    return fetch(backend_url + 'login', {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow',
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.status == 401) {
            console.log("Wrong user credentials");
            return null;
        }
        return response.json();
    })
    .catch(function(err) {
        console.info("err:", err );
     });
}


async function makeEventChanelRequest(token) {
   return new EventSourcePolyfill(backend_url + 'events' + '?heartbeat=true', {
       headers: {
          'Authorization': token,
          'Content-Type': 'application/json'
       },
   });
}

async function getUserAccountInfoRequest(token) {
    return fetch(backend_url + 'account', {
        method: 'GET',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            Authorization: token,
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .catch(function(err) {
        console.info("err:", err );
    });
}

async function getUserSessionsRequest(token) {
    return fetch(backend_url + 'sessions' + '?page=0&size=1000', {
        method: 'GET',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            Authorization: token
        }
    })
    .then(response => response.json())
    .then(data => data.content)
    .catch(function(err) {
        console.info("err:", err );
    });
}


async function getUserSessionRequest(token, id) {
    return fetch(backend_url + 'sessions' + '/' + id, {
        method: 'GET',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            Authorization: token
        }
    })
    .then(response => response.json())
    .catch(function(err) {
        console.info("err:", err );
    });
}


async function createUserSessionRequest(name, tabsString, activeTab, mode, token) {
    data={'name': name, 'tabs' : tabsString, 'activeTab': activeTab, 'mode': mode}
    return fetch(backend_url + 'sessions', {
        method: 'POST',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            'Content-Type': 'application/json',
            Authorization: token
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.status == 415) {
            console.log("Wrong data");
            return null;
        }
        return response.json();
    })
    .catch(function(err) {
        console.info("err:", err );
        return null;
    });
}

async function removeSessionByIdRequest(id, token) {
    return fetch(backend_url + 'sessions' + '/' + id, {
        method: 'DELETE',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            'Content-Type': 'application/json',
            Authorization: token
        }
    })
    .then(response => {
        if (response.status == 400) {
            console.log("Error 400");
            return null;
        }
        return response;
    })
    .catch(function(err) {
        console.info("err:", err );
        return null;
    });
}

async function updateSessionRequest(id, name, stringTabs, activeTab, mode, updateId, token) {
    data={'name': name, 'tabs' : stringTabs, 'activeTab': activeTab, 'mode': mode}

    return fetch(backend_url + 'sessions' + '/' + id + '?updateId=' + updateId, {
        method: 'PUT',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            'Content-Type': 'application/json',
            Authorization: token
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.status == 400) {
            console.log("Error 400");
            return null;
        }
        return response.json();
    })
    .catch(function(err) {
        console.info("err:", err );
        return null;
    });
}


async function updateUserRequest(name, email, defaultMode, token) {
    data={'name': name, 'email': email, 'defaultMode': defaultMode}

    return fetch(backend_url + 'account' , {
        method: 'PUT',
        mode: 'cors',
        redirect: 'follow',
        headers: {
            'Content-Type': 'application/json',
            Authorization: token
        },
        body: JSON.stringify(data)
    })
    .then(response => {
        if (response.status == 400) {
            console.log("Error 400");
            return null;
        }
        return response.json();
    })
    .catch(function(err) {
        console.info("err:", err );
        return null;
    });
}

async function main() {
//    let current_user = new User();
//    await current_user.login("test5@mail.ru", "12345");
    let tab1 = new Tab(0, "https://www.youtube.com", null);
    let tab2 = new Tab(1, "https://drive.google.com", null);
//    let new_session = new Session("example_session4", [tab1, tab2], 0,
//        SessionMode.DETACHED, current_user.storage);
//    let result = await current_user.addSession(new_session);
//    console.log("result", result);
////    current_user.setUpdateSessionHandler(1, console.log);
//    result = await new_session.changeName("Change session name");
//    console.log("result", result);
//
//    result = await new_session.changeName("second name");
//    console.log("result", result);
//
//    console.log("new_session", new_session);
//    console.log("new_session tab", new_session.getTab(2));
//    console.log("user", current_user);

    var user2 = new User();
//    var result = await user2.login("test5@mail.ru", "12345");
    var result = await user2.login("test8@mail.ru", "12345");
    console.log("result", result);
    console.log("user", user2);

    function function_w(si) {
        window.alert(si);
    }


    var session = new Session("newnewsession", [tab1, tab2], 0,
          SessionMode.DETACHED, user2.storage);
    console.log("session", session);
    result = await user2.addSession(session);

    console.log("session", session);
    user2.setUpdateSessionHandler(session.id, function_w);

    let tab3 = new Tab(2, "https://kellllll.com", null);

    await session.addTab(tab3)
    console.log("synchronize", await session.synchronize());

    await session.addTab(tab3)
    console.log("synchronize", await session.synchronize());

    await session.addTab(tab3)
    console.log("synchronize", await session.synchronize());
    console.log("sleeping");

    await session.addTab(tab3)
    console.log("synchronize", await session.synchronize());

    console.log("modified", await session.wasModifiedOnServer());


    result = await user2.changeName("newName22");
    console.log("synchronize", await session.synchronize());

//    result = await current_user.removeSession(24);
//    console.log("user", current_user);
}


//console.log("requests.js:main() is invoked");
//main()
//.catch(e => {
//  console.log('There has been a problem with your fetch operation: ' + e.message);
//});
