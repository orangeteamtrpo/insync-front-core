class Session {
    constructor(name, tabs, activeTab, mode, storage) {
        this.name = name;
        this.tabs = tabs;
        this.activeTab = activeTab;
        this.mode = mode;
        this.storage = storage;
        this.updateIds = [];
    }


    async addTab(tab, forcedSynchronize = false) {
        var oldSessionTabs = [...this.tabs];
        this.tabs.push(tab);
        if (this.mode == SessionMode.ATTACHED || forcedSynchronize) {
            return await this.synchronize()
            .then((requestResult) => {
                if (!requestResult) {
                    this.tabs = oldSessionTabs;
                    return false;
                } else {
                    return true;
                }
            });
        } else {
            return true;
        }
    }

    async removeTab(id, forcedSynchronize = false) {
        var oldSessionTabs = [...this.tabs];
        var i = 0;
        var hasThisTab = false;
        this.tabs.forEach(element => {
            if(element.id == id) {
                this.tabs.splice(i, 1);
                hasThisTab = true;
            }
            i++;
        });

        if (!hasThisTab) {
            return false;
        }

        if (this.mode == SessionMode.ATTACHED || forcedSynchronize) {
            return await this.synchronize()
            .then((requestResult) => {
                if (!requestResult) {
                    this.tabs = oldSessionTabs;
                    return false;
                } else {
                    return true;
                }
            });
        } else {
            return true;
        }
    }

    getTab(id) {
        for (let i = 0; i < this.tabs.length; ++i) {
            if(this.tabs[i].id == id) {
                return element;
            }
        }

        return null;
    }

    setTabs(tabs) {
        this.tabs = tabs;
    }

    setActiveTab(tab) {
        this.activeTab = tab;
    }

    show(showTabFunction) {
        this.tabs.forEach((x) => showTabFunction(x));
    }

    setActionHandler(handler) {
        this.actionHandler = handler;
        return true;
    }

    async setMode(mode) {
        var oldMode = this.mode;
        this.mode = mode;

        return await this.synchronize()
            .then((requestResult) => {
                if (!requestResult) {
                    this.mode = oldMode;
                    return false;
                } else {
                    return true;
                }
            });
    }

    async changeName(newName) {
        var oldName = this.name;
        this.name = newName;

        return await this.synchronize()
            .then((requestResult) => {
                if (!requestResult) {
                    this.name = oldName;
                    return false;
                } else {
                    return true;
                }
            });
    }

    updateFields(data, startTabIndex) {
        for (var key in data){
            if (key != "tabs") {
                this[key] = data[key];
            } else {
                var newTabs = [];
                data.tabs.forEach(url => {
                    var newTab = new Tab(startTabIndex[0], url, null);
                    startTabIndex[0]++;
                    newTabs.push(newTab);
                })
                this.tabs = newTabs;
            }
        }

        return true;
    }

    equalsData(data) {
        for (var key in ["tabs", "mode", "name", "id", "activeTab"]) {
            if (key == "tabs") {
                if (this[key].length != data[key].length) {
                    console.log(key + " length not equal");
                    return false;
                }
                for (var i = 0; i < this[key].length; i++) {
                    if (this[key][i].url != data[key][i]) {
                        console.log(this[key][i].url + " not equals " + data[key][i]);
                        return false;
                    }
                }
            } else {
                if (this[key] != data[key]) {
                    return false;
                }
            }
        }

        return true;
    }


    async wasModifiedOnServer() {
        return await getUserSessionRequest(this.storage.getToken(), this.id)
            .then((data) => {
//                console.log("mod on server" + Date.parse(data.lastModified));
//                console.log("mod local" + Date.parse(this.lastModified));
                return Date.parse(data.lastModified) > Date.parse(this.lastModified);
            });
    }

    // update session on server.
    // returns true if succeed, false otherwise
    async synchronize() {
        var newUpdateId = Math.floor(Math.random() * 1000000);;
        this.updateIds.push(newUpdateId);
        return await updateSessionRequest(this.id, this.name, tabsToString(this.tabs),
            this.activeTab, this.mode, newUpdateId, this.storage.getToken())
            .then((data) => {return this.equalsData(data)});
    }
}

const Origin = {
    "LOCAL" : 0,
    "REMOTE" : 1
};

const SessionMode = {
    DETACHED : 1,
    ATTACHED : 2
};