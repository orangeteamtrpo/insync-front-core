class Tab {
    constructor(id, url, actionHandler) {
        this.id = id;
        this.url = url;
        this.actionHandler = actionHandler;
    }

    setUrl(url) {
        this.url = url;
    }
}

function tabsToString(tabs) {
    var string_tabs = [];
    console.log("tabs", tabs);
    tabs.forEach((tab) => string_tabs.push(tab.url));
    return string_tabs;
}


