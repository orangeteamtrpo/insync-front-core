
class User {
    constructor(id, name, sessions, email, storage, password="def passsword",
            currentSession = null, defaultMode = SessionMode.DETACHED) {
        this.id = id;
        this.name = name;
        this.sessions = sessions;
        this.currentSession = currentSession;
        this.email = email;
        this.password = password;
        this.newTabId = [0];
        this.defaultMode = defaultMode;
    }

    async login(email, password) {
        data = await loginRequest(email, password);
        if (data == null) {
            return false;
        }
        this.storage =  new LocalStorage(data.token);
        return await this.updateUserData();
    }

    async register(username, email, password) {
        data = await registerRequest(username, email, password);
        if (data == null) {
            return false;
        }
        this.storage =  new LocalStorage(data.token);
        return await this.updateUserData();
    }

    async updateUserData() {
        var user_data = await getUserAccountInfoRequest(this.storage.getToken());
        var sessions_info = await getUserSessionsRequest(this.storage.getToken());
        var sessions = [];
        sessions_info.forEach(element => {
            var new_session = new Session();
            new_session.updateFields(element, this.newTabId);
            new_session.storage = this.storage;
            sessions.push(new_session);
        });
        this.id = user_data.id;
        this.name = user_data.name;
        this.sessions = sessions;
        this.email = user_data.email;
        return true;
    }

    async addSession(session) {
        return await createUserSessionRequest(session.name, tabsToString(session.tabs),
            session.activeTab, session.mode, this.storage.getToken())
        .then(result => {
            if (result != null) {
               var dataStoredCorrectly = true; //session.equalsData(result);
               if (dataStoredCorrectly) {
                   session.updateFields(result, this.newTabId);
                   this.sessions.push(session);
               }
               return dataStoredCorrectly;
            }
            return false;
        });
    }


    // same as addSession, but use method PUT. Need when the session exists to rewrite it
    async updateSession(session) {
        var newUpdateId = Math.floor(Math.random() * 1000000);;
        session.updateIds.push(newUpdateId);
        return updateSessionRequest(session.id, session.name, tabsToString(session.tabs),
            session.activeTab, session.mode, newUpdateId, session.storage.getToken())
            .then((data) => {return this.equalsData(data)})
        .then(result => {
            if (result != null) {
               var dataStoredCorrectly = true; //session.equalsData(result);
               if (dataStoredCorrectly) {
                   session.updateFields(result, this.newTabId);
               }
               return dataStoredCorrectly;
            }
            return false;
        });
    }


    async removeSession(id) {
        return removeSessionByIdRequest(id, this.storage.getToken())
        .then(result => {
            if (result != null) {
                let i = 0;
                this.sessions.forEach(element => {
                    if(element.id == id) {
                        this.sessions.splice(i, 1);
                    }
                    i++;
                });
                return true;
            }
            return false;
        });
    }

    async changeName(newName) {
        if (this.defaultMode == null) {
            this.defaultMode = SessionMode.DETACHED;
        }
        return updateUserRequest(newName, this.email, this.defaultMode, this.storage.getToken())
        .then(result => {
            if (result != null) {
                this.name = newName;
                return true;
            }
            return false;
        });
    }

    async sessionUpdateCallback(sessionId, sessionUpdate) {
        for (let i = 0; i < this.sessionWatchUpdate.length; i++) {
            if(this.sessionWatchUpdate[i] == sessionId) {
                var session = await this.getSession(sessionId);
                if (session != null) {
                    for (let j = 0; j < session.updateIds.length; j++) {
                         if (session.updateIds[j] == parseInt(sessionUpdate)) {
                            console.log("Get my own update with session id "
                                + sessionId + " and update " + sessionUpdate);
                            return;
                         }
                    }
                    var handler = this.handlers.get(sessionId);
                    handler(sessionId);
                }
            }
        };
    }

    async setUpdateSessionHandler(sessionId, handler) {
        if (this.sessionWatchUpdate == null) {
            this.sessionWatchUpdate = [];
            this.handlers = new Map();
            this.updateChanel = await makeEventChanelRequest(this.storage.getToken());
//            this.updateChanel.onerror = function(error) {
//                console.error("⛔ EventSource failed: ", error);
//            };
            this.updateChanel.addEventListener('session-update', e => {
                data = JSON.parse(e.data);
//                console.log('Update data', data);
                this.sessionUpdateCallback(data.sessionId, data.eventId);
            });
        }
        this.sessionWatchUpdate.push(sessionId);
        this.handlers.set(sessionId, handler);
        return true;
    }

    async removeUpdateSessionHandler(sessionId) {
        if (this.sessionWatchUpdate != null) {
            this.handlers.delete(sessionId);
            var index = this.sessionWatchUpdate.indexOf(sessionId);
            if (index !== -1) {
              this.sessionWatchUpdate.splice(index, 1);
            }
        }
    }


    async getSession(id) {
        for (let i = 0; i < this.sessions.length; i++) {
            if(this.sessions[i].id == id) {
                return this.sessions[i];
            }
        };

        return null;
    }
}

