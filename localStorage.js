
function formatToken(token) {
    return 'Bearer ' + token;
}

class LocalStorage {
   constructor(token) {
       this.token = formatToken(token);
   }

   getToken() {
       return this.token;
   }
}